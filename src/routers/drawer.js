import {createDrawerNavigator} from "react-navigation-drawer";
import {createAppContainer} from "react-navigation";

import Home from "../screens/home";
import About from "../screens/about";

const routes = createDrawerNavigator({
    home : Home,
    about : About
},{
    initialRouteName:'home',
    drawerBackgroundColor:"red",
    drawerPosition:"right",
    drawerWidth:"70%"
});

export default createAppContainer(routes);