import React from 'react';
import AppRouter from "./src/routers/drawer";

export default function App() {
  return (
    <AppRouter />
  );
}
